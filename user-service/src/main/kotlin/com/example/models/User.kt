package com.example.models

import org.jetbrains.exposed.sql.Table


object User : Table() {
    val id = integer("id").primaryKey().autoIncrement()
    val name = varchar("name", 100)
    val email = varchar("email", 500)
    val credit = double("credit")
    val updatedAt = long("updated_at")
}