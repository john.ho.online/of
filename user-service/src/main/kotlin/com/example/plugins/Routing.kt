package com.example.plugins

import com.example.services.UserService
import com.example.services.UserServiceImpl
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*

fun Application.configureRouting() {
    val userService = UserServiceImpl()
    install(StatusPages) {
        exception<NoSuchElementException> { cause ->
            call.respond(HttpStatusCode.NotFound, cause.message.toString())
        }
        exception<ConcurrentModificationException> { cause ->
            call.respond(HttpStatusCode.PreconditionFailed, cause.message.toString())
        }
        exception<IllegalArgumentException> { cause ->
            call.respond(HttpStatusCode.BadRequest, cause.message.toString())
        }
    }
    routing {
        createUser(userService)
    }
}

fun Routing.createUser(userService: UserService) {
    post("/user") {
        val user = call.receive<com.example.dtos.User>()
        val userId = userService.createUser(user)
        call.respond(HttpStatusCode.Created, userId)
    }
    get("/user/{id}") {
        val id = call.parameters["id"]
        id?.let {
            val user = userService.getUser(id.toInt())
            user?.let {
                call.respond(HttpStatusCode.OK, user)
            } ?: call.respond(HttpStatusCode.NotFound)

        } ?: call.respond(HttpStatusCode.BadRequest)
    }

    post("/user/purchase") {
        val userPurchase = call.receive<com.example.dtos.UserPurchase>()
        userService.updateCredit(userPurchase.id, userPurchase.amount)
        call.respond(HttpStatusCode.NoContent)
    }
}