package com.example.services

import com.example.models.User
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import java.lang.IllegalArgumentException

class UserServiceImpl : UserService {
    override fun createUser(user: com.example.dtos.User): Int = transaction {
        User.insert {
            it[User.name] = user.name
            it[User.email] = user.email
            it[User.credit] = user.credit
            it[User.updatedAt] = System.currentTimeMillis()
        } get User.id
    }

    override fun updateCredit(id: Int, amount: Double): Unit = transaction {
        val user = getUser(id)
        user?.let {
            val credit = user.credit - amount
            if (amount.equals(0) || credit < 0) {
                throw IllegalArgumentException("Amount not enough")
            }
            val updated =
                User.update({ (User.id eq id) and (User.updatedAt eq user.updatedAt) and (User.credit eq user.credit) }) {
                    it[User.credit] = credit
                    it[User.updatedAt] = System.currentTimeMillis()
                }
            if (updated < 1) {
                throw ConcurrentModificationException("Another purchase is being processed")
            }
        } ?: throw NoSuchElementException("User not found")

    }

    override fun getUser(id: Int): com.example.dtos.User? = transaction {
        User.select { User.id eq id }.map {
            com.example.dtos.User(
                it[User.id], it[User.name], it[User.email], it[User.credit], it[User.updatedAt]
            )
        }.singleOrNull()
    }
}