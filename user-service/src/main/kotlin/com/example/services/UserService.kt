package com.example.services

import com.example.dtos.User

interface UserService {
    fun createUser(user: User) : Int
    fun updateCredit(id: Int, amount: Double)
    fun getUser(id: Int): User?
}