package com.example.dtos

data class UserPurchase (
    val id: Int,
    val amount: Double
) 