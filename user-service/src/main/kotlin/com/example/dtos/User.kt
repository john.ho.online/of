package com.example.dtos

data class User (var id: Int, val name: String, val email: String, val credit: Double, val updatedAt: Long)