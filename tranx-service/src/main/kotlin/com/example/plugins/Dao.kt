package com.example.plugins

import com.example.models.Tranx
import io.ktor.application.*
import org.jetbrains.exposed.sql.SchemaUtils.create
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.transaction

fun Application.configureDao() {
    Database.connect("jdbc:h2:mem:test1;DB_CLOSE_DELAY=-1;", driver = "org.h2.Driver")
    transaction {
        create(Tranx)
    }

}