package com.example.plugins

import com.example.client.apis.UserApi
import com.example.client.infrastructure.Serializer
import com.example.services.TranxService
import com.example.services.TranxServiceImpl
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*

fun Application.configureRouting() {
    val userApi = UserApi(basePath = "http://user-service:8080")
    val tranxService = TranxServiceImpl(userApi)
    install(StatusPages) {
        exception<NoSuchElementException> { cause ->
            call.respond(HttpStatusCode.NotFound, cause.message.toString())
        }
        exception<ConcurrentModificationException> { cause ->
            call.respond(HttpStatusCode.PreconditionFailed, cause.message.toString())
        }
        exception<IllegalArgumentException> { cause ->
            call.respond(HttpStatusCode.BadRequest, cause.message.toString())
        }
        exception<com.example.client.infrastructure.ClientException> { cause ->
            call.respond(HttpStatusCode.BadRequest, Serializer.jacksonObjectMapper.writeValueAsString(cause.response))
        }
        exception<com.example.client.infrastructure.ServerException> { cause ->
            call.respond(HttpStatusCode.InternalServerError, "Internal Server Error")
        }
    }
    routing {
        tranx(tranxService)
    }
}

fun Routing.tranx(tranxService: TranxService) {
    post("/tranx") {
        val tranx = call.receive<com.example.dtos.TranxRequest>()
        val tranxId = tranxService.createTranx(tranx)
        call.respond(HttpStatusCode.Created, tranxId)
    }
    get("/tranx") {
        val userId = call.parameters["userId"]
        userId?.let {
            val listTranx = tranxService.listTranx(userId.toInt())
            call.respond(HttpStatusCode.OK, listTranx)
        } ?: call.respond(HttpStatusCode.BadRequest)
    }
}
