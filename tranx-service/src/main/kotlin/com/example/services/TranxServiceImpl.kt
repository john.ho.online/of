package com.example.services

import com.example.client.apis.UserApi
import com.example.client.models.UserPurchase
import com.example.models.Tranx
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

class TranxServiceImpl : TranxService {
    private val userApi: UserApi

    constructor(userApi: UserApi) {
        this.userApi = userApi
    }

    override fun createTranx(tranx: com.example.dtos.TranxRequest): Int = transaction {
        userApi.userPurchase(UserPurchase(tranx.userId, tranx.amount))
        Tranx.insert {
            it[Tranx.userId] = tranx.userId
            it[Tranx.amount] = tranx.amount
            it[Tranx.updatedAt] = System.currentTimeMillis()
        } get Tranx.id
    }

    override fun listTranx(userId: Int): List<com.example.dtos.Tranx> = transaction {
        Tranx.select { Tranx.userId eq userId }.map {
            com.example.dtos.Tranx(
                it[Tranx.id], it[Tranx.userId], it[Tranx.amount], it[Tranx.updatedAt]
            )
        }
    }
}