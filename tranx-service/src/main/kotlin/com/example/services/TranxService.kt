package com.example.services

import com.example.dtos.TranxList
import com.example.dtos.TranxRequest

interface TranxService {
    fun createTranx(tranx: TranxRequest): Int
    fun listTranx(id: Int): List<com.example.dtos.Tranx>
}