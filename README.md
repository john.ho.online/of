## System Design
### Simple Solution
![Simple Solution](./imgs/of.jpg)


- Simple Solution is built on top of Ktor, SQL DB, Swagger, Restful API
- **Edge Server** : orchestrate requests before they go into internal services including authentication, authorization, traffic control, transformation,..
- **User Service**: allow to create/get user's info
- **Transaction Service**: Create/manage user's transactions
- To asure the consistency of system, we're simply applying optimistic locking mechanism which is using Unix Timestamp as record's version. 
- Pros: Easy to implementation
- Cons: Fast-fail

### Advanced Solution (Not Implemented Yet)
![Advanced Solution](./imgs/of2.jpg)


- Advanced Solution is built on top of Ktor, SQL DB, Swagger, Kafka, Redis and **Asynchonous Restful APIs**. With this appach, we removed dependency between User Service and Transaction service by using **Apache Kafka**. Transaction Service can easily check a status of a transaction via a kafka topic, transaction-result topic for exxample.
- Intead of using optimistic locking mechanism, we use **Redis** as distributed locking system to ensure system consistency
- Pros: Complexity
- Cons: Asynchonous, Scalable

## Getting started

Install docker https://docs.docker.com/get-docker/

Install docker-compose https://docs.docker.com/compose/install/

Install maven http://maven.apache.org/install.html

Run `run.sh file`
