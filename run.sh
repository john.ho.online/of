#!/bin/bash
docker-compose down
cd user-service
mvn clean install
cd ../tranx-service
mvn clean install
cd ../
docker-compose build
docker-compose up
